/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <gconf/gconf-client.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomeui/gnome-about.h>
#include <libgnomeui/gnome-app-helper.h>

#include "vgdefaultview.h"
#include "menu-utils.h"
#include "alleyoop.h"
#include "process.h"
#include "prefs.h"


enum {
	VALGRIND_TOOL_MEMCHECK,
	VALGRIND_TOOL_ADDRCHECK,
	VALGRIND_TOOL_CACHEGRIND,
	VALGRIND_TOOL_HELGRIND,
};

static struct {
	const char *name;
	int tool;
} valgrind_tools[] = {
	{ "memcheck",   VALGRIND_TOOL_MEMCHECK   },
	{ "addrcheck",  VALGRIND_TOOL_ADDRCHECK  },
	{ "cachegrind", VALGRIND_TOOL_CACHEGRIND },
	{ "helgrind",   VALGRIND_TOOL_HELGRIND   },
	{ NULL,         VALGRIND_TOOL_MEMCHECK   },
};

static void alleyoop_class_init (AlleyoopClass *klass);
static void alleyoop_init (Alleyoop *alleyoop);
static void alleyoop_destroy (GtkObject *obj);
static void alleyoop_finalize (GObject *obj);

/*static void tree_row_expanded (GtkTreeView *treeview, GtkTreeIter *root, GtkTreePath *path, gpointer user_data);
  static gboolean tree_button_press (GtkWidget *treeview, GdkEventButton *event, gpointer user_data);*/

static gboolean io_ready_cb (GIOChannel *gio, GIOCondition condition, gpointer user_data);


static GnomeAppClass *parent_class = NULL;


GType
alleyoop_get_type (void)
{
	static GType type = 0;
	
	if (!type) {
		static const GTypeInfo info = {
			sizeof (AlleyoopClass),
			NULL, /* base_class_init */
			NULL, /* base_class_finalize */
			(GClassInitFunc) alleyoop_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (Alleyoop),
			0,    /* n_preallocs */
			(GInstanceInitFunc) alleyoop_init,
		};
		
		type = g_type_register_static (GNOME_TYPE_APP, "Alleyoop", &info, 0);
	}
	
	return type;
}

static void
alleyoop_class_init (AlleyoopClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkObjectClass *gtk_object_class = GTK_OBJECT_CLASS (klass);
	
	parent_class = g_type_class_ref (GNOME_TYPE_APP);
	
	object_class->finalize = alleyoop_finalize;
	gtk_object_class->destroy = alleyoop_destroy;
}

static void
alleyoop_init (Alleyoop *grind)
{
	grind->argv = NULL;
	grind->srcdir = NULL;
	
	grind->view = NULL;
	
	grind->gio = NULL;
	grind->watch_id = 0;
	grind->pid = (pid_t) -1;
	
	grind->toolbar_run = NULL;
	grind->toolbar_kill = NULL;
	
	grind->about = NULL;
	grind->prefs = NULL;
}

static void
alleyoop_finalize (GObject *obj)
{
	Alleyoop *grind = (Alleyoop *) obj;
	
	if (grind->gio)
		g_io_channel_unref (grind->gio);
	
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}

static void
alleyoop_destroy (GtkObject *obj)
{
	Alleyoop *grind = (Alleyoop *) obj;
	
	if (grind->prefs) {
		gtk_widget_destroy (grind->prefs);
		grind->prefs = NULL;
	}
	
	GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}


static char **
run_prompt_argv (Alleyoop *grind)
{
	GtkWidget *vbox, *hbox, *entry;
	GtkWidget *dialog, *widget;
	GError *err = NULL;
	const char *command;
	char **argv = NULL;
	int argc;
	
	dialog = gtk_dialog_new_with_buttons (_("Run Executable..."), (GtkWindow *) grind,
					      GTK_DIALOG_DESTROY_WITH_PARENT,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_EXECUTE, GTK_RESPONSE_OK, NULL);
	
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width ((GtkContainer *) vbox, 6);
	
	hbox = gtk_hbox_new (FALSE, 6);
	
	widget = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION, GTK_ICON_SIZE_DIALOG);
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	
	widget = gtk_label_new (_("Enter the path to an executable and\nany arguments you wish to pass to it."));
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	
	gtk_widget_show (hbox);
	gtk_box_pack_start ((GtkBox *) vbox, hbox, FALSE, FALSE, 0);
	
	/* FIXME: use a GnomeFileEntry instead? */
	entry = gtk_entry_new ();
	gtk_widget_show (entry);
	gtk_box_pack_start ((GtkBox *) vbox, entry, FALSE, FALSE, 0);
	
	gtk_widget_show (vbox);
	gtk_box_pack_start ((GtkBox *) ((GtkDialog *) dialog)->vbox, vbox, FALSE, FALSE, 0);
	
 reprompt:
	if (gtk_dialog_run ((GtkDialog *) dialog) == GTK_RESPONSE_OK) {
		command = gtk_entry_get_text ((GtkEntry *) entry);
		if (!g_shell_parse_argv (command, &argc, &argv, &err)) {
			GtkWidget *msg;
			
			msg = gtk_message_dialog_new ((GtkWindow *) dialog, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						      GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", err->message);
			g_error_free (err);
			err = NULL;
			
			gtk_dialog_run ((GtkDialog *) msg);
			gtk_widget_destroy (msg);
			
			argv = NULL;
			goto reprompt;
		}
	}
	
	gtk_widget_destroy (dialog);
	
	return argv;
}

static void
file_run_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	GError *err = NULL;
	GtkWidget *dialog;
	
	if (grind->argv == NULL) {
		char **argv;
		
		if (!(argv = run_prompt_argv (grind)))
			return;
		
		g_object_set_data_full ((GObject *) grind, "argv", argv, (GDestroyNotify) g_strfreev);
		grind->argv = (const char **) argv;
	}
	
	alleyoop_run (grind, &err);
	
	if (err != NULL) {
		dialog = gtk_message_dialog_new ((GtkWindow *) grind, GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", err->message);
		
		gtk_dialog_run ((GtkDialog *) dialog);
		gtk_widget_destroy (dialog);
		
		g_error_free (err);
	}
}

static void
file_kill_cb (GtkWidget *widget, gpointer user_data)
{
	alleyoop_kill (user_data);
}

static void
open_ok_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	const char *filename;
	GtkWidget *filesel;
	int fd;
	
	filesel = g_object_get_data ((GObject *) grind, "open_filesel");
	filename = gtk_file_selection_get_filename ((GtkFileSelection *) filesel);
	
	if (filename != NULL) {
		if ((fd = open (filename, O_RDONLY)) != -1) {
			vg_tool_view_connect ((VgToolView *) grind->view, fd);
			
			grind->pid = (pid_t) -1;
			grind->gio = g_io_channel_unix_new (fd);
			grind->watch_id = g_io_add_watch (grind->gio, G_IO_IN | G_IO_HUP, io_ready_cb, grind);
			
			gtk_widget_set_sensitive (grind->toolbar_run, FALSE);
			gtk_widget_set_sensitive (grind->toolbar_kill, TRUE);
		} else {
			GtkWidget *dialog;
			
			dialog = gtk_message_dialog_new ((GtkWindow *) grind, GTK_DIALOG_DESTROY_WITH_PARENT,
							 GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
							 _("Could not load `%s': %s"), filename,
							 g_strerror (errno));
			
			g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
			
			gtk_widget_show (dialog);
		}
	}
	
	gtk_widget_destroy (filesel);
}

static void
open_notify_cb (Alleyoop *grind, GObject *deadbeef)
{
	g_object_set_data ((GObject *) grind, "open_filesel", NULL);
}

static void
file_open_cb (GtkWidget *widget, gpointer user_data)
{
	GtkFileSelection *filesel;
	
	if ((filesel = g_object_get_data ((GObject *) user_data, "open_filesel"))) {
		gdk_window_raise (((GtkWidget *) filesel)->window);
		return;
	}
	
	filesel = (GtkFileSelection *) gtk_file_selection_new (_("Load Valgrind log..."));
	g_signal_connect (filesel->ok_button, "clicked", G_CALLBACK (open_ok_cb), user_data);
	g_object_set_data ((GObject *) user_data, "open_filesel", filesel);
	
	g_signal_connect_swapped (filesel->cancel_button, "clicked",
				  G_CALLBACK (gtk_widget_destroy),
				  (gpointer) filesel); 
	
	g_object_weak_ref ((GObject *) filesel, (GWeakNotify) open_notify_cb, user_data);
	
	gtk_widget_show ((GtkWidget *) filesel);
}

static void
save_valgrind_log (Alleyoop *grind, const char *filename, int flags)
{
	GtkWidget *dialog;
	int response;
	int fd;
	
	flags |= O_WRONLY | O_CREAT | O_TRUNC;
	
 retry:
	if ((fd = open (filename, flags, 0666)) != -1) {
		int errnosav;
		
		if (vg_tool_view_save_log ((VgToolView *) grind->view, fd) == -1) {
			errnosav = errno;
			close (fd);
			unlink (filename);
			errno = errnosav;
			goto exception;
		}
		
		close (fd);
	} else if (flags & O_EXCL && errno == EEXIST) {
		dialog = gtk_message_dialog_new ((GtkWindow *) grind, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO,
						 _("Could not save to `%s': %s\nOverwrite?"), filename,
						 g_strerror (errno));
		
		response = gtk_dialog_run ((GtkDialog *) dialog);
		gtk_widget_destroy (dialog);
		
		if (response == GTK_RESPONSE_YES) {
			flags &= ~O_EXCL;
			goto retry;
		}
	} else {
	exception:
		
		dialog = gtk_message_dialog_new ((GtkWindow *) grind, GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
						 _("Could not save to `%s': %s"), filename,
						 g_strerror (errno));
		
		g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
		
		gtk_widget_show (dialog);
	}
}

static void
save_ok_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	const char *filename;
	GtkWidget *filesel;
	
	filesel = g_object_get_data ((GObject *) grind, "save_filesel");
	filename = gtk_file_selection_get_filename ((GtkFileSelection *) filesel);
	
	if (filename != NULL) {
		g_free (g_object_get_data ((GObject *) grind, "save_filename"));
		g_object_set_data_full ((GObject *) grind, "save_filename", g_strdup (filename), (GDestroyNotify) g_free);
	}
	
	save_valgrind_log (grind, filename, O_EXCL);
	
	gtk_widget_destroy (filesel);
}

static void
save_notify_cb (Alleyoop *grind, GObject *deadbeef)
{
	g_object_set_data ((GObject *) grind, "save_filesel", NULL);
}


static void
file_save_as_cb (GtkWidget *widget, gpointer user_data)
{
	GtkFileSelection *filesel;
	
	if ((filesel = g_object_get_data ((GObject *) user_data, "save_filesel"))) {
		gdk_window_raise (((GtkWidget *) filesel)->window);
		return;
	}
	
	filesel = (GtkFileSelection *) gtk_file_selection_new (_("Save Valgrind log..."));
	g_signal_connect (filesel->ok_button, "clicked", G_CALLBACK (save_ok_cb), user_data);
	g_object_set_data ((GObject *) user_data, "save_filesel", filesel);
	
	g_signal_connect_swapped (filesel->cancel_button, "clicked",
				  G_CALLBACK (gtk_widget_destroy),
				  (gpointer) filesel); 
	
	g_object_weak_ref ((GObject *) filesel, (GWeakNotify) save_notify_cb, user_data);
	
	gtk_widget_show ((GtkWidget *) filesel);
}

static void
file_save_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	const char *filename;
	
	filename = g_object_get_data ((GObject *) grind, "save_filename");
	if (filename != NULL)
		save_valgrind_log (grind, filename, 0);
	else
		file_save_as_cb (widget, user_data);
}

static void
file_quit_cb (GtkWidget *widget, gpointer user_data)
{
	gtk_main_quit ();
}

static void
edit_cut_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	vg_tool_view_cut ((VgToolView *) grind->view);
}

static void
edit_copy_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	vg_tool_view_copy ((VgToolView *) grind->view);
}

static void
edit_paste_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	vg_tool_view_paste ((VgToolView *) grind->view);
}

static void
edit_clear_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	vg_tool_view_clear ((VgToolView *) grind->view);
}

static void
edit_prefs_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	gtk_widget_show (grind->prefs);
}

static void
edit_rules_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	vg_tool_view_show_rules ((VgToolView *) grind->view);
}

static void
tools_default_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	grind->tool = NULL;
}

static void
tools_addrcheck_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	grind->tool = "addrcheck";
}

#if 0
static void
tools_cachegrind_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	grind->tool = "cachegrind";
}
#endif

static void
tools_helgrind_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	grind->tool = "helgrind";
}

static void
about_destroy (GObject *obj, GObject *deadbeef)
{
	Alleyoop *grind = (Alleyoop *) obj;
	
	grind->about = NULL;
}

static const char *authors[] = {
	"Jeffrey Stedfast <fejj@novell.com>",
	NULL
};

/* This string should be replaced with the name of the translator */
static const char translation[] = N_("Jeffrey Stedfast");

static void
help_about_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind;
	
	grind = (Alleyoop *) user_data;
	if (grind->about == NULL) {
		grind->about = gnome_about_new ("Alleyoop", VERSION, "Copyright 2003-2009 Jeffrey Stedfast <fejj@novell.com>",
						_("Alleyoop is a Valgrind front-end for the GNOME environment."),
						(const char **) authors, NULL, _(translation), NULL);
		
		g_object_weak_ref ((GObject *) grind->about, (GWeakNotify) about_destroy, grind);
		
		gtk_widget_show (grind->about);
	}
	
	gdk_window_raise (grind->about->window);
}

static GnomeUIInfo file_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("_Run"), NULL, G_CALLBACK (file_run_cb), NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GTK_STOCK_EXECUTE, 'r', GDK_CONTROL_MASK, NULL },
	{ GNOME_APP_UI_ITEM, N_("_Kill"), NULL, G_CALLBACK (file_kill_cb), NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GTK_STOCK_CANCEL, 'k', GDK_CONTROL_MASK, NULL },
	GNOMEUIINFO_MENU_OPEN_ITEM (G_CALLBACK (file_open_cb), NULL),
	GNOMEUIINFO_MENU_SAVE_ITEM (G_CALLBACK (file_save_cb), NULL),
	GNOMEUIINFO_MENU_SAVE_AS_ITEM (G_CALLBACK (file_save_as_cb), NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_QUIT_ITEM (G_CALLBACK (file_quit_cb), NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[] = {
	GNOMEUIINFO_MENU_CUT_ITEM (G_CALLBACK (edit_cut_cb), NULL),
	GNOMEUIINFO_MENU_COPY_ITEM (G_CALLBACK (edit_copy_cb), NULL),
	GNOMEUIINFO_MENU_PASTE_ITEM (G_CALLBACK (edit_paste_cb), NULL),
	GNOMEUIINFO_MENU_CLEAR_ITEM (G_CALLBACK (edit_clear_cb), NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo settings_menu[] = {
	GNOMEUIINFO_MENU_PREFERENCES_ITEM (G_CALLBACK (edit_prefs_cb), NULL),
	{ GNOME_APP_UI_ITEM, N_("Suppressions"), N_("View/Edit Suppressions"), G_CALLBACK (edit_rules_cb),
	  NULL, NULL, GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
	GNOMEUIINFO_END
};

static GnomeUIInfo tools_menu_items[] = {
	{ GNOME_APP_UI_ITEM, "Memcheck", NULL, G_CALLBACK (tools_default_cb), NULL,
	  NULL, GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
	{ GNOME_APP_UI_ITEM, "Addrcheck", NULL, G_CALLBACK (tools_addrcheck_cb), NULL,
	  NULL, GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
	/*{ GNOME_APP_UI_ITEM, "Cachegrind", NULL, G_CALLBACK (tools_cachegrind_cb), NULL,
	  NULL, GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },*/
	{ GNOME_APP_UI_ITEM, "Helgrind", NULL, G_CALLBACK (tools_helgrind_cb), NULL,
	  NULL, GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
	GNOMEUIINFO_END
};

static GnomeUIInfo tools_menu[] = {
	GNOMEUIINFO_RADIOLIST (tools_menu_items),
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_MENU_ABOUT_ITEM (G_CALLBACK (help_about_cb), NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo alleyoop_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE (file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE (edit_menu),
	GNOMEUIINFO_MENU_SETTINGS_TREE (settings_menu),
	GNOMEUIINFO_SUBTREE (N_("Tool"), tools_menu),
	GNOMEUIINFO_MENU_HELP_TREE (help_menu),
	GNOMEUIINFO_END
};

static GtkWidget *
alleyoop_toolbar_new (Alleyoop *grind)
{
	GtkWidget *toolbar, *image;
	GtkToolbarChild *child;
	
	toolbar = gtk_toolbar_new ();
	
	image = gtk_image_new_from_stock (GTK_STOCK_EXECUTE, GTK_ICON_SIZE_LARGE_TOOLBAR);
	gtk_widget_show (image);
	gtk_toolbar_append_item ((GtkToolbar *) toolbar, _("Run"), _("Run Program"), NULL,
				 image, G_CALLBACK (file_run_cb), grind);
	
	image = gtk_image_new_from_stock (GTK_STOCK_CANCEL, GTK_ICON_SIZE_LARGE_TOOLBAR);
	gtk_widget_show (image);
	gtk_toolbar_append_item ((GtkToolbar *) toolbar, _("Kill"), _("Kill Program"), NULL,
				 image, G_CALLBACK (file_kill_cb), grind);
	
	gtk_toolbar_insert_stock ((GtkToolbar *) toolbar, GTK_STOCK_OPEN, _("Open Log File"),
				  NULL, G_CALLBACK (file_open_cb), grind, 2);
	gtk_toolbar_insert_stock ((GtkToolbar *) toolbar, GTK_STOCK_SAVE, _("Save Log File"),
				  NULL, G_CALLBACK (file_save_cb), grind, 3);
	
	gtk_widget_show (toolbar);
	
	child = ((GtkToolbar *) toolbar)->children->data;
	grind->toolbar_run = child->widget;
	
	child = ((GtkToolbar *) toolbar)->children->next->data;
	grind->toolbar_kill = child->widget;
	
	return toolbar;
}

static void
prefs_response_cb (GtkDialog *dialog, int response, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	gtk_widget_hide (grind->prefs);
}

static gboolean
prefs_delete_event (GtkWidget *widget, gpointer user_data)
{
	gtk_widget_hide (widget);
	
	return TRUE;
}

GtkWidget *
alleyoop_new (const char *tool, const char **argv, const char **srcdir)
{
	GtkWidget *widget;
	Alleyoop *grind;
	char *title;
	int i;
	
	grind = g_object_new (ALLEYOOP_TYPE, NULL);
	
	if (argv && argv[0] != NULL)
		title = g_strdup_printf ("Alleyoop - [%s]", argv[0]);
	else
		title = g_strdup ("Alleyoop");
	
	gnome_app_construct ((GnomeApp *) grind, "alleyoop", title);
	gtk_window_set_default_size ((GtkWindow *) grind, 300, 400);
	gnome_app_enable_layout_config ((GnomeApp *) grind, TRUE);
	g_free (title);
	
	grind->tool = tool;
	grind->argv = argv;
	grind->srcdir = srcdir;
	
	/* now construct the UI */
	gnome_app_create_menus_with_data ((GnomeApp *) grind, alleyoop_menu, grind);
	
	if (tool != NULL) {
		widget = NULL;
		
		for (i = 0; valgrind_tools[i].name != NULL; i++) {
			if (!strcmp (valgrind_tools[i].name, tool)) {
				widget = tools_menu_items[i].widget;
				break;
			}
		}
		
		if (widget != NULL)
			gtk_check_menu_item_set_active ((GtkCheckMenuItem *) widget, TRUE);
	}
	
	widget = alleyoop_toolbar_new (grind);
	gnome_app_set_toolbar ((GnomeApp *) grind, (GtkToolbar *) widget);
	gtk_widget_set_sensitive (grind->toolbar_run, TRUE);
	gtk_widget_set_sensitive (grind->toolbar_kill, FALSE);
	
	grind->view = widget = vg_default_view_new ();
	vg_tool_view_set_argv ((VgToolView *) widget, argv);
	vg_tool_view_set_srcdir ((VgToolView *) widget, srcdir);
	gtk_widget_show (widget);
	gnome_app_set_contents ((GnomeApp *) grind, widget);
	
	/* create the prefs dialog (we just don't display it) */
	grind->prefs = alleyoop_prefs_new ();
	g_signal_connect (grind->prefs, "response", G_CALLBACK (prefs_response_cb), grind);
	g_signal_connect (grind->prefs, "delete-event", G_CALLBACK (prefs_delete_event), grind);
	
	return (GtkWidget *) grind;
}

static gboolean
io_ready_cb (GIOChannel *gio, GIOCondition condition, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	if ((condition & G_IO_IN) && vg_tool_view_step ((VgToolView *) grind->view) <= 0) {
		alleyoop_kill (grind);
		grind->watch_id = 0;
		return FALSE;
	}
	
	if (condition & G_IO_HUP) {
		alleyoop_kill (grind);
		grind->watch_id = 0;
		return FALSE;
	}
	
	return TRUE;
}

void
alleyoop_run (Alleyoop *grind, GError **err)
{
	char logfd_arg[30];
	GPtrArray *args;
	int logfd[2];
	char **argv;
	int i;
	
	if (!grind->argv || !grind->argv[0])
		return;
	
	if (grind->pid != (pid_t) -1)
		return;
	
	if (pipe (logfd) == -1)
		return;
	
	args = alleyoop_prefs_create_argv ((AlleyoopPrefs *) grind->prefs, grind->tool);
	
	sprintf (logfd_arg, "--log-fd=%d", logfd[1]);
	g_ptr_array_add (args, logfd_arg);
	
	g_ptr_array_add (args, "--");
	for (i = 0; grind->argv[i] != NULL; i++)
		g_ptr_array_add (args, (char *) grind->argv[i]);
	g_ptr_array_add (args, NULL);
	
	argv = (char **) args->pdata;
	
	grind->pid = process_fork (argv[0], argv, TRUE, logfd[1], NULL, NULL, NULL, err);
	if (grind->pid == (pid_t) -1) {
		close (logfd[0]);
		close (logfd[1]);
		return;
	}
	
	g_ptr_array_free (args, TRUE);
	
	close (logfd[1]);
	
	vg_tool_view_connect ((VgToolView *) grind->view, logfd[0]);
	
	grind->gio = g_io_channel_unix_new (logfd[0]);
	grind->watch_id = g_io_add_watch (grind->gio, G_IO_IN | G_IO_HUP, io_ready_cb, grind);
	
	gtk_widget_set_sensitive (grind->toolbar_run, FALSE);
	gtk_widget_set_sensitive (grind->toolbar_kill, TRUE);
}


void
alleyoop_kill (Alleyoop *grind)
{
	vg_tool_view_disconnect ((VgToolView *) grind->view);
	
	if (grind->gio) {
		g_io_channel_close (grind->gio);
		g_io_channel_unref (grind->gio);
		grind->watch_id = 0;
		grind->gio = NULL;
	}
	
	if (grind->pid != (pid_t) -1) {
		process_kill (grind->pid);
		grind->pid = (pid_t) -1;
	}
	
	gtk_widget_set_sensitive (grind->toolbar_run, TRUE);
	gtk_widget_set_sensitive (grind->toolbar_kill, FALSE);
}
