/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifndef __ALLEYOOP_H__
#define __ALLEYOOP_H__

#include <gtk/gtk.h>
#include <libgnomeui/gnome-app.h>

#include "vgerror.h"
#include "process.h"

#define ALLEYOOP_TYPE            (alleyoop_get_type ())
#define ALLEYOOP(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ALLEYOOP_TYPE, Alleyoop))
#define ALLEYOOP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), ALLEYOOP_TYPE, AlleyoopClass))
#define IS_ALLEYOOP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ALLEYOOP_TYPE))
#define IS_ALLEYOOP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), ALLEYOOP_TYPE))
#define ALLEYOOP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), ALLEYOOP_TYPE, AlleyoopClass))

typedef struct _Alleyoop Alleyoop;
typedef struct _AlleyoopClass AlleyoopClass;

struct _Alleyoop {
	GnomeApp parent_object;
	
	const char *tool;
	const char **argv;
	const char **srcdir;
	
	GtkWidget *view;
	
	GIOChannel *gio;
	guint watch_id;
	pid_t pid;
	
	GtkWidget *toolbar_run;
	GtkWidget *toolbar_kill;
	
	GtkWidget *about;      /* About dialog */
	GtkWidget *prefs;      /* Prefs dialog */
};

struct _AlleyoopClass {
	GnomeAppClass parent_class;
	
};


GType alleyoop_get_type (void);

GtkWidget *alleyoop_new (const char *tool, const char **argv, const char **srcdir);

void alleyoop_run (Alleyoop *alleyoop, GError **err);
void alleyoop_kill (Alleyoop *alleyoop);

#endif /* __ALLEYOOP_H__ */
