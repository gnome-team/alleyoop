/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include <gnome.h>
#include <glade/glade.h>
#include <gconf/gconf.h>

#include "getopts.h"
#include "alleyoop.h"
#include "vgstrpool.h"


static gboolean
alleyoop_close (GtkWidget *widget, gpointer user_data)
{
	gtk_main_quit ();
	
	return FALSE;
}

static void
add_subdirs (GPtrArray *srcdir, GPtrArray *gc, const char *topsrcdir)
{
	struct dirent *dent;
	GPtrArray *subdirs;
	struct stat st;
	char *path;
	DIR *dir;
	int i;
	
	if (!(dir = opendir (topsrcdir)))
		return;
	
	subdirs = g_ptr_array_new ();
	
	while ((dent = readdir (dir))) {
		if (dent->d_name[0] == '.')
			continue;
		
		path = g_strdup_printf ("%s/%s", topsrcdir, dent->d_name);
		if (stat (path, &st) != -1 && S_ISDIR (st.st_mode)) {
			g_ptr_array_add (subdirs, path);
		} else {
			g_free (path);
		}
	}
	
	closedir (dir);
	
	for (i = 0; i < subdirs->len; i++) {
		path = subdirs->pdata[i];
		g_ptr_array_add (srcdir, path);
		g_ptr_array_add (gc, path);
		add_subdirs (srcdir, gc, path);
	}
	
	g_ptr_array_free (subdirs, TRUE);
}


static const char *tool = NULL;
static GPtrArray *srcdir, *gc;

static void
shutdown (void)
{
	size_t i;
	
	for (i = 0; i < gc->len; i++)
		g_free (gc->pdata[i]);
	g_ptr_array_free (gc, TRUE);
	
	g_ptr_array_free (srcdir, TRUE);
}

static int
display_help (GetOptsContext *ctx, GetOptsOption *opt, const char *arg, void *valuep)
{
	printf ("Usage: %s [OPTIONS...] [program [options]]\n\n", PACKAGE);
	getopts_print_help (ctx);
	shutdown ();
	exit (0);
	
	return 0;
}

static int
display_version (GetOptsContext *ctx, GetOptsOption *opt, const char *arg, void *valuep)
{
	fputs (PACKAGE " " VERSION "\n", stdout);
	shutdown ();
	exit (0);
	
	return 0;
}

static int
include_dir (GetOptsContext *ctx, GetOptsOption *opt, const char *arg, void *valuep)
{
	g_ptr_array_add (srcdir, (char *) arg);
	
	return 0;
}

static int
recurse_dir (GetOptsContext *ctx, GetOptsOption *opt, const char *arg, void *valuep)
{
	g_ptr_array_add (srcdir, (char *) arg);
	add_subdirs (srcdir, gc, arg);
	
	return 0;
}

static int
use_tool (GetOptsContext *ctx, GetOptsOption *opt, const char *arg, void *valuep)
{
	if (!g_ascii_strcasecmp (arg, "memcheck")) {
		/* default */
		tool = NULL;
	} else if (!g_ascii_strcasecmp (arg, "addrcheck")) {
		tool = "addrcheck";
	} else if (!g_ascii_strcasecmp (arg, "cachegrind")) {
		/*tool = "cachegrind";*/
		fprintf (stderr, "%s is currently an unsupported tool\n", arg);
	} else if (!g_ascii_strcasecmp (arg, "helgrind")) {
		/*tool = "helgrind";*/
		fprintf (stderr, "%s is currently an unsupported tool\n", arg);
	} else {
		fprintf (stderr, "Unknown tool: %s\n", arg);
		shutdown ();
		exit (0);
	}
	
	return 0;
}

#define HELP_TEXT      N_("Display help and quit")
#define VERSION_TEXT   N_("Display version and quit")
#define INCLUDE_TEXT   N_("Add <dir> to the list of directories to search for source files")
#define RECURSIVE_TEXT N_("Recursively add <dir> and all subdirectories to the list of directories to search for source files")
#define TOOL_TEXT      N_("Specify the default Valgrind tool to use")

static GetOptsOption options[] = {
	{ "help",      'h',  GETOPTS_NO_ARG | GETOPTS_ARG_CUSTOM, HELP_TEXT,      NULL,     'h', display_help,    NULL },
	{ "version",   'v',  GETOPTS_NO_ARG | GETOPTS_ARG_CUSTOM, VERSION_TEXT,   NULL,     'v', display_version, NULL },
	{ "include",   'I',  GETOPTS_REQUIRED_CUSTOM_ARG,         INCLUDE_TEXT,   "<dir>",  'I', include_dir,     NULL },
	{ "recursive", 'R',  GETOPTS_REQUIRED_CUSTOM_ARG,         RECURSIVE_TEXT, "<dir>",  'R', recurse_dir,     NULL },
	{ "tool",      '\0', GETOPTS_REQUIRED_CUSTOM_ARG,         TOOL_TEXT,      "<name>", 't', use_tool,        NULL },
	GETOPTS_TABLE_END
};

int main (int argc, char **argv)
{
	GnomeProgram *program;
	GetOptsContext *ctx;
	GtkWidget *alleyoop;
	const char **args;
	char *srcdir_env;
	int i, n;
	
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	
	gc = g_ptr_array_new ();
	srcdir = g_ptr_array_new ();
	
	for (i = 0; i < G_N_ELEMENTS (options); i++)
		options[i].description = _(options[i].description);
	
	ctx = getopts_context_new (argc, argv, options, GETOPTS_FLAG_ALLOW_SINGLE_DASH | GETOPTS_FLAG_BREAK_ON_FIRST_NONARG);
	getopts_parse_args (ctx);
	args = getopts_get_args (ctx, &n);
	
	/* if there are no args, set args to NULL so that the Run command will prompt. */
	if (n == 0)
		args = NULL;
	
	program = gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE, argc, argv,
				      GNOME_PARAM_HUMAN_READABLE_NAME, _("Alleyoop"),
				      GNOME_PARAM_APP_DATADIR, PACKAGE_DATA_DIR,
				      NULL);
	
	if ((srcdir_env = getenv ("ALLEYOOP_INCLUDE_PATH"))) {
		/* add our environment to the list of srcdir paths */
		char *path, *p;
		
		path = srcdir_env = g_strdup (srcdir_env);
		do {
			if ((p = strchr (path, ':'))) {
				*p++ = '\0';
				if (path[0] != '\0')
					g_ptr_array_add (srcdir, path);
			} else {
				g_ptr_array_add (srcdir, path);
				break;
			}
			
			path = p;
		} while (path != NULL);
	}
	
	g_ptr_array_add (srcdir, NULL);
	
	glade_init ();
	vg_strpool_init ();
	alleyoop = alleyoop_new (tool, args, (const char **) srcdir->pdata);
	g_signal_connect (alleyoop, "delete-event", G_CALLBACK (alleyoop_close), NULL);
	gtk_widget_show (alleyoop);
	
	gtk_main ();
	
	getopts_context_free (ctx, TRUE);
	g_object_unref (program);
	vg_strpool_shutdown ();
	g_free (srcdir_env);
	shutdown ();
	
	return 0;
}
