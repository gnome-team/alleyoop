/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <gconf/gconf-client.h>
#include <libgnome/gnome-i18n.h>

#include "prefs.h"
#include "vgtoolprefs.h"
#include "vggeneralprefs.h"
#include "vghelgrindprefs.h"
#include "vgmemcheckprefs.h"
#include "vgcachegrindprefs.h"


#define EDITOR_KEY      "/apps/alleyoop/editor"
#define NUM_LINES_KEY   "/apps/alleyoop/num-lines"

enum {
	PAGE_GENERAL    = 0,
	PAGE_MEMCHECK   = 1,
	PAGE_ADDRCHECK  = PAGE_MEMCHECK,
	PAGE_CACHEGRIND = 2,
	PAGE_HELGRIND   = 3,
};

static GType page_types[] = {
	(GType) 0, /* general */
	(GType) 0, /* memcheck (addrcheck) */
	(GType) 0, /* cachegrind */
	(GType) 0, /* helgrind */
};

#define NUM_PREFS_PAGES 4

static void alleyoop_prefs_class_init (AlleyoopPrefsClass *klass);
static void alleyoop_prefs_init (AlleyoopPrefs *prefs);
static void alleyoop_prefs_destroy (GtkObject *obj);
static void alleyoop_prefs_finalize (GObject *obj);


static GtkDialogClass *parent_class = NULL;


GType
alleyoop_prefs_get_type (void)
{
	static GType type = 0;
	
	if (!type) {
		static const GTypeInfo info = {
			sizeof (AlleyoopPrefsClass),
			NULL, /* base_class_init */
			NULL, /* base_class_finalize */
			(GClassInitFunc) alleyoop_prefs_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (AlleyoopPrefs),
			0,    /* n_preallocs */
			(GInstanceInitFunc) alleyoop_prefs_init,
		};
		
		type = g_type_register_static (GTK_TYPE_DIALOG, "AlleyoopPrefs", &info, 0);
	}
	
	return type;
}

static void
alleyoop_prefs_class_init (AlleyoopPrefsClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkObjectClass *gtk_object_class = GTK_OBJECT_CLASS (klass);
	
	parent_class = g_type_class_ref (GTK_TYPE_DIALOG);
	
	object_class->finalize = alleyoop_prefs_finalize;
	gtk_object_class->destroy = alleyoop_prefs_destroy;
	
	if (page_types[0] == (GType) 0) {
		page_types[PAGE_GENERAL] = VG_TYPE_GENERAL_PREFS;
		page_types[PAGE_MEMCHECK] = VG_TYPE_MEMCHECK_PREFS;
		page_types[PAGE_CACHEGRIND] = VG_TYPE_CACHEGRIND_PREFS;
		page_types[PAGE_HELGRIND] = VG_TYPE_HELGRIND_PREFS;
	}
}


static gboolean
entry_changed (GtkEntry *entry, GdkEvent *event, const char *key)
{
	GConfClient *gconf;
	const char *str;
	
	gconf = gconf_client_get_default ();
	
	str = gtk_entry_get_text (entry);
	gconf_client_set_string (gconf, key, str ? str : "", NULL);
	
	g_object_unref (gconf);
	
	return FALSE;
}

static gboolean
spin_changed (GtkSpinButton *spin, GdkEvent *event, const char *key)
{
	GConfClient *gconf;
	int num;
	
	gconf = gconf_client_get_default ();
	
	num = gtk_spin_button_get_value_as_int (spin);
	gconf_client_set_int (gconf, key, num, NULL);
	
	g_object_unref (gconf);
	
	return FALSE;
}

static void
alleyoop_prefs_init (AlleyoopPrefs *prefs)
{
	GtkWidget *vbox, *hbox, *label;
	GtkWidget *notebook;
	GConfClient *gconf;
	int num, i;
	char *str;
	
	gconf = gconf_client_get_default ();
	
	gtk_window_set_title ((GtkWindow *) prefs, _("Alleyoop Preferences"));
	gtk_window_set_type_hint ((GtkWindow *) prefs, GDK_WINDOW_TYPE_HINT_NORMAL);
	gtk_dialog_add_button ((GtkDialog *) prefs, GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
	gtk_box_set_spacing ((GtkBox *) ((GtkDialog *) prefs)->vbox, 6);
	gtk_container_set_border_width ((GtkContainer *) prefs, 6);
	
	vbox = gtk_vbox_new (FALSE, 6);
	
	hbox = gtk_hbox_new (FALSE, 6);
	label = gtk_label_new (_("Editor:"));
	gtk_widget_show (label);
	gtk_box_pack_start ((GtkBox *) hbox, label, FALSE, FALSE, 0);
	
	if (!(str = gconf_client_get_string (gconf, EDITOR_KEY, NULL)))
		str = g_strdup ("emacsclient -n +${lineno} \"${filename}\"");
	prefs->editor = (GtkEntry *) gtk_entry_new ();
	gtk_entry_set_text (prefs->editor, str);
	g_signal_connect (prefs->editor, "focus-out-event", G_CALLBACK (entry_changed), EDITOR_KEY);
	gtk_widget_show ((GtkWidget *) prefs->editor);
	gtk_box_pack_start ((GtkBox *) hbox, (GtkWidget *) prefs->editor, TRUE, TRUE, 0);
	g_free (str);
	
	gtk_widget_show (hbox);
	gtk_box_pack_start ((GtkBox *) vbox, hbox, FALSE, FALSE, 0);
	
	hbox = gtk_hbox_new (FALSE, 6);
	label = gtk_label_new (_("Preview"));
	gtk_widget_show (label);
	gtk_box_pack_start ((GtkBox *) hbox, label, FALSE, FALSE, 0);
	
	num = gconf_client_get_int (gconf, NUM_LINES_KEY, NULL);
	prefs->numlines = (GtkSpinButton *) gtk_spin_button_new_with_range (0, (gdouble) INT_MAX, 1);
	gtk_spin_button_set_digits (prefs->numlines, 0);
	gtk_spin_button_set_numeric (prefs->numlines, TRUE);
	gtk_spin_button_set_value (prefs->numlines, (gdouble) num);
	g_signal_connect (prefs->numlines, "focus-out-event", G_CALLBACK (spin_changed), NUM_LINES_KEY);
	gtk_widget_show ((GtkWidget *) prefs->numlines);
	gtk_box_pack_start ((GtkBox *) hbox, (GtkWidget *) prefs->numlines, FALSE, FALSE, 0);
	
	label = gtk_label_new (_("lines above and below the target line."));
	gtk_widget_show (label);
	gtk_box_pack_start ((GtkBox *) hbox, label, FALSE, FALSE, 0);
	
	gtk_widget_show (hbox);
	gtk_box_pack_start ((GtkBox *) vbox, hbox, FALSE, FALSE, 0);
	
	notebook = gtk_notebook_new ();
	
	for (i = 0; i < NUM_PREFS_PAGES; i++) {
		prefs->pages[i] = g_object_new (page_types[i], NULL);
		label = gtk_label_new (((VgToolPrefs *) prefs->pages[i])->label);
		gtk_container_set_border_width ((GtkContainer *) prefs->pages[i], 6);
		gtk_widget_show (prefs->pages[i]);
		gtk_widget_show (label);
		
		gtk_notebook_append_page ((GtkNotebook *) notebook, prefs->pages[i], label);
	}
	
	gtk_widget_show (notebook);
	gtk_box_pack_start ((GtkBox *) vbox, notebook, FALSE, FALSE, 0);
	
	gtk_widget_show (vbox);
	gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (prefs)->vbox), vbox);
	
	g_object_unref (gconf);
}

static void
alleyoop_prefs_finalize (GObject *obj)
{
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}

static void
alleyoop_prefs_destroy (GtkObject *obj)
{
	GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}


GtkWidget *
alleyoop_prefs_new (void)
{
	AlleyoopPrefs *prefs;
	
	prefs = g_object_new (ALLEYOOP_TYPE_PREFS, NULL);
	
	return (GtkWidget *) prefs;
}


GPtrArray *
alleyoop_prefs_create_argv (AlleyoopPrefs *prefs, const char *tool)
{
	GPtrArray *argv;
	int page;
	
	argv = g_ptr_array_new ();
	g_ptr_array_add (argv, VALGRIND_PATH);
	
	if (tool != NULL) {
		if (!strcmp (tool, "memcheck")) {
			g_ptr_array_add (argv, "--tool=memcheck");
			page = PAGE_MEMCHECK;
		} else if (!strcmp (tool, "addrcheck")) {
			g_ptr_array_add (argv, "--tool=addrcheck");
			page = PAGE_ADDRCHECK;
		} else if (!strcmp (tool, "cachegrind")) {
			g_ptr_array_add (argv, "--tool=cachegrind");
			page = PAGE_CACHEGRIND;
		} else if (!strcmp (tool, "helgrind")) {
			g_ptr_array_add (argv, "--tool=helgrind");
			page = PAGE_HELGRIND;
		} else {
			g_assert_not_reached ();
		}
	} else {
		/* default tool */
		g_ptr_array_add (argv, "--tool=memcheck");
		page = PAGE_MEMCHECK;
	}
	
	/* next, apply the general prefs */
	vg_tool_prefs_get_argv ((VgToolPrefs *) prefs->pages[PAGE_GENERAL], tool, argv);
	
	/* finally, apply the current view's prefs */
	vg_tool_prefs_get_argv ((VgToolPrefs *) prefs->pages[page], tool, argv);
	
	return argv;
}
